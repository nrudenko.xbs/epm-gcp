terraform {
  required_providers {
    google = {
      version = "~> 3.45.0"
      source  = "registry.terraform.io/hashicorp/google"
    }
    null   = {
      source  = "registry.terraform.io/hashicorp/null"
      version = "~> 2.1"
    }
  }
}

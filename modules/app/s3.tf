resource "google_storage_bucket" "REGIONAL" {
  name = var.bucket_name
  storage_class = "REGIONAL"
  location = var.region
  force_destroy = true 
}

resource "google_storage_bucket_acl" "image-store-acl" {
  bucket = google_storage_bucket.REGIONAL.name
  default_acl = "publicRead"
}

variable "region" {
  default = "us-central1"
}
variable "project" {
  default = ""
}
variable "network_name" {
  default = "default"
}
variable "database" {
  default = "bookshelf"
}
variable "db_tier" {
  default = "db-f1-micro"
}
variable "machine_type" {
  default = "f1-micro"
}
variable "install_script_src_path" {
  default = "scripts/startup-script.sh"
}
variable "image" {
  default = "debian-cloud/debian-9"
}
variable "service_account_email" {
  default = ""
}
variable "scopes" {
  default = ["userinfo-email", "cloud-platform"]
}
variable "tag" {
  default = "http-server"
}
variable "myuser" {
  default = "root"
}
variable "mypass" {
  default = ""
}
variable "db_instance_name" {
  default = "sql0"
}
variable "bucket_name" {
  default = "bucket12qw"
}


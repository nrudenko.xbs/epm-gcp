data "template_file" "default" {
  template = file(var.install_script_src_path)
  vars     = {
    myuser     = var.myuser
    mypass     = var.mypass
    database   = var.database
    connection = google_sql_database_instance.instance.connection_name 
    bucket     = var.bucket_name 
  }
}

resource "google_compute_instance_template" "compute_instance_template" {

    name                    = "${var.project}-template"

    project                 = var.project
    region                  = var.region
    machine_type            = var.machine_type


    metadata_startup_script = data.template_file.default.rendered


    network_interface {
        network             = var.network_name
    }

    disk {
        auto_delete     = true
        boot            = true
        source_image    = var.image
    }

    service_account {
        email   = var.service_account_email
        scopes  = var.scopes
    }

    tags = [
        var.tag
    ]

    lifecycle {
        ignore_changes = [
            network_interface
        ]
        create_before_destroy = true
    }

}

resource "google_compute_instance_group_manager" "mig" {
  name               = substr("my-mig-${md5(google_compute_instance_template.compute_instance_template.name)}", 0, 63)
  base_instance_name = "mig-instance"
  
  version {
      instance_template  = google_compute_instance_template.compute_instance_template.self_link
  }

  zone               = "us-central1-f"
  target_size        = 5
  wait_for_instances = true

  timeouts {
    create = "15m"
    update = "15m"
  }
  
  lifecycle {
    create_before_destroy = true
  }

  depends_on = [google_sql_database_instance.instance]
}

resource "google_compute_autoscaler" "scale" {
  name    = "my-region-autoscaler"
  project = var.project
  zone    = "us-central1-f"
  target  = google_compute_instance_group_manager.mig.self_link
  
  autoscaling_policy {
    max_replicas = 5
    min_replicas = 2
    cooldown_period = 60
    
    cpu_utilization {
      target = 0.6
    }
  }
}

resource "google_compute_instance_group_named_port" "my_port" {
  group = google_compute_instance_group_manager.mig.instance_group
  zone  = "us-central1-f"

  name = "http"
  port = 5100
}

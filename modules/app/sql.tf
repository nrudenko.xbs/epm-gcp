resource "google_compute_global_address" "private_ip_address" {

  name          = "private-ip-address"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = var.network_name
}

resource "google_service_networking_connection" "private_vpc_connection" {

  network                 = var.network_name
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}


# create database instance
resource "google_sql_database_instance" "instance" {
        name = var.db_instance_name
        region = var.region
        deletion_protection = false
        depends_on = [google_service_networking_connection.private_vpc_connection]

        settings {
                tier = var.db_tier

                database_flags {
                        name  = "slow_query_log"
                        value = "on"
                }

                ip_configuration {
                        ipv4_enabled = "false"
                        private_network = "projects/${var.project}/global/networks/${var.network_name}"
                }
        }
}

resource "google_sql_database" "database" {
  name     = var.database
  instance = google_sql_database_instance.instance.name
}

resource "google_sql_user" "users" {
  name     = "root"
  instance = google_sql_database_instance.instance.name
  host     = "%"
  password = var.mypass
}

resource "google_compute_address" "static_ip" {
  name   = "whitelisted-static-ip"
  region = var.region
}

data "google_compute_address" "static_ip" {
  name    = "whitelisted-static-ip"
  region  = var.region
}

resource "google_compute_router_nat" "cluster-nat" {
  name                               = "${var.company}-nat"
  router                             = google_compute_router.router.name
  region                             = google_compute_router.router.region
  nat_ip_allocate_option             = "MANUAL_ONLY"
  nat_ips                            = [google_compute_address.static_ip.self_link]
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_PRIMARY_IP_RANGES"
}

resource "google_compute_router" "router" {
  name    = "${var.company}-router"
  network = google_compute_network.vpc.name
  region  = var.region
  bgp {
    asn   = 64514
  }
}

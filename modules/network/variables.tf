variable "env" {
  default = "dev"
}
variable "company" { 
  default = "company-name"
}
variable "region" {
  default = "us-central1"
}
variable "service_port" {
  default = "5100"
}
variable "service_port_name" {
  default = "http"
}
variable "backend" {
  default = ""
}
variable "project" {
  default = ""
}

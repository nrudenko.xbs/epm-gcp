#!/bin/bash
set -v

export MYUSER=${myuser}
export MYPASS=${mypass}
export DATABASE=${database}
export CONNECTION=${connection}
export BUCKET=${bucket}

# Talk to the metadata server to get the project id
PROJECTID=$(curl -s "http://metadata.google.internal/computeMetadata/v1/project/project-id" -H "Metadata-Flavor: Google")


# add ansible repo
echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" >> /etc/apt/sources.list
# Install dependencies from apt
apt-get install -yq dirmngr
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
apt-get update
apt-get install -yq \
    git  ansible=2.9.16-1ppa~trusty 

# Get the source code from the Google Cloud Repository
# git requires $HOME and it's not set during the startup script.
export HOME=/root
git config --global credential.helper gcloud.sh
git clone --recurse-submodules -j2  https://source.developers.google.com/p/$PROJECTID/r/hello-ansible
ansible-galaxy collection install community.general  -p /hello-ansible/collections

# run ansuble
ansible-playbook -vvv /hello-ansible/main.yaml --extra-vars "user=$MYUSER password=$MYPASS database=$DATABASE connection=$CONNECTION bucket=$BUCKET project=$PROJECTID" --vault-password-file /hello-ansible/ansible_pass/.ansible_pass 


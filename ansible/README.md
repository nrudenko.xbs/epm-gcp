Automation playbooks to deploy "getting-started-python" app on gcp infrastructure
This playbooks were designed to be launched from terraform startup-script, so most variables should be defined there.
There are two variables needs to be defined in vars/varstore.yml which is encrypted with ansible-vault:
  secret: The secret key is used by Flask to encrypt session cookies
  venv: The path where virtual environment will be created




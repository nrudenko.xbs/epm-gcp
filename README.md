This terraform code designed to deploy gcp infrastructure for "getting-started-python" application.
All needed variables can be defined in variables.tf or directly in main.tf
You must set at least your own project_id to run this project.
The mypass variable which if for sql database password should be defined from cli like #terraform apply -var='mypass="your desired database password"'



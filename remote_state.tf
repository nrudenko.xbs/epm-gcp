terraform {
  backend "gcs" {
    bucket   = "terraform12"
    prefix   = "terraform/state"
  }
}

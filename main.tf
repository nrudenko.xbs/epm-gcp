provider "google" {
}

module "network" {
  source            = "./modules/network"
  backend           = module.app.instance_group
  project           = var.project_id
  region            = var.project_region
  env               = var.env
  company           = var.company
  service_port      = var.service_port
  service_port_name = var.service_port_name
}

module "app" {
  source                  = "./modules/app"
  project                 = var.project_id
  region                  = var.project_region
  service_account_email   = module.service_accounts.service_account.email 
  network_name            = module.network.network_name
  database                = var.database
  db_tier                 = var.db_tier  
  machine_type            = var.machine_type 
  install_script_src_path = var.install_script_src_path
  image                   = var.image  
  scopes                  = var.scopes
  tag                     = var.tag
  db_instance_name        = var.db_instance_name
  bucket_name             = var.bucket_name
  mypass                  = var.mypass 
}

module "service_accounts" {
  source        = "github.com/terraform-google-modules/terraform-google-service-accounts"
  project_id    = var.project_id
  prefix        = var.prefix
  names         = ["single-account"]
  project_roles = ["${var.project_id}=>roles/editor"]
  display_name  = "Single Account"
  description   = "Single Account Description"
}


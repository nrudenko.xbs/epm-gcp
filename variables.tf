variable "project_id" {
  default = "idyllic-mantis-299418"
  description = "The project ID to host the network in"
}

variable "project_region" {
  default     = "us-central1"
  description = "The region to host the infrastructure in"
}

variable "prefix" {
  type        = string
  description = "Prefix applied to service account names."
  default     = ""
}

variable "env" {
  default = "dev"
}
variable "company" { 
  default = "company-name"
}
variable "service_port" {
  default = "5100"
}
variable "service_port_name" {
  default = "http"
}

variable "database" {
  default = "bookshelf"
}
variable "db_tier" {
  default = "db-f1-micro"
}
variable "machine_type" {
  default = "f1-micro"
}
variable "install_script_src_path" {
  default = "scripts/startup-script.sh"
}
variable "image" {
  default = "debian-cloud/debian-9"
}
variable "scopes" {
  default = ["userinfo-email", "cloud-platform"]
}
variable "tag" {
  default = "http-server"
}
variable "db_instance_name" {
 default = "sql8"
}
variable "bucket_name" {
  default = "bucket12qw"
}
variable "mypass" {
  default = ""
}
